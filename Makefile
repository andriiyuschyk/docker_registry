clear:
	docker rm -vf $(docker ps -aq)
	docker rmi -f $(docker images -aq)

build:
	docker-compose build
	docker-compose create

start:
	make build
	docker-compose start